#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include<signal.h>

#define NUM_CHILD 3

int main()
{
    printf(" Create %d child process \n", NUM_CHILD);

    int status = 0;
    int counter = 0;

    for(int i = 0; i < NUM_CHILD; i++)//creat child process in the loop
    {
        pid_t pid = fork();//creat child process function

        if(pid == 0)//child process
        {
            printf(" I am a child process ,pid [%d] , ppid [%d]\n ", getpid(), getppid());//get child procee's id and parent process id
            printf(" Child [%d] start \n", getpid());
            sleep(10);//after 10 second , this child process will terminited
            printf(" Child [%d] excution has completed \n", getpid());//give information that child process completed
            exit(0);

        }
        else
        {
            if(pid > 0)//parent process
            {
                printf(" parent pid  [%d]\n ", getpid());//get parent pid

            }
            else
            {
                printf(" fork() failed \n");
                kill(0, SIGTERM);//terminited all child process
                exit(1);
            }
        }
        sleep(1);//break between creat new child process
        printf(" break one second \n\n");
    }

    printf(" %d childs process have been created \n", NUM_CHILD);

    while(1)//check if the creating process finished
    {
        pid_t child_pid = wait(&status);
        if(child_pid == -1//finished to creat child process
    {
        printf("parents[%d]:excution has completed\n ", getpid());//print out the massage of parent completed
            break;
        }
        else
        {
            counter++;// counter the number of child process terminated normally
        }
    }

    printf("\n");
    printf(" No more child process\n");
    printf(" %d process terminated normally\n ", counter);

    return 0;
}

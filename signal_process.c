#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include<signal.h>

#define NUM_CHILD 5//define how mang child process will be created
int mark =0;//gouble various check if send massahe to terminated the process

void interrupt()//parent process get the signal that keyboard interrupt occurance
{
    printf(" Parent [%d] : sending SIGTERM signal . \n ", getpid());//print out info about receiving the keyboard interrupt
    mark = 1;//when the keyboard interrupt occurance, parent process should send SIGTERM signal to all created child process

}
void termination()
{
    printf("Child[%d] : received SIGTERM signal, terminating \n", getpid());//print a message of the termination of child process.
}
int main()
{
    printf(" Create %d child process \n", NUM_CHILD);
    int number=0;
    int status = 0;
    int counter = 0;
    int term=0;
    int array[NUM_CHILD];//set array to save pid
    for(int i = 0; i < NUM_CHILD; i++)//creat child process in the loop
    {
        pid_t pid = fork();//creat child process function
        if(pid == 0)//child process
        {
            signal (SIGINT,SIG_IGN);//ignore the signal of any interrupt
            signal (SIGTERM,termination);//child process handle the SIGTEAM

            printf(" I am a child process,pid [%d], ppid [%d]\n ", getpid(), getppid());//get child procee's id and parent process id
            printf(" Child [%d] start to be created \n", getpid());
            sleep(10);//after 10 second , this child process will terminited
            printf(" Child [%d] excution has completed \n", getpid());//give information that child process completed
            exit(0);
        }
        else
        {
            if(pid > 0)//parent process
            {
                array[number]=pid;//save the id of created child process
                number++;

                for(int j = 1; j < NSIG; j++) // NSIG : The number of signal which defined
                {
                    signal(j,SIG_IGN);//force ignoring of all signals
                }
                signal(SIGINT, interrupt);//parent process get the keyboard interrupt
                signal(SIGCHLD, SIG_DFL);// restore the default handler for SIGCHLD signal
                printf(" parent pid  [%d]\n ", getpid());//get parent pid
            }
            else
            {
                printf(" fork() failed \n");
                kill(0, SIGTERM);//terminited all child process
                exit(1);
            }
        }
        sleep(1);// break between creat new child process
        printf(" break one second \n\n");
         if(mark)//check if the interrupt happen by the global various
                {
                    printf(" Parent [%d]  interrupt of the creation process.\n", getpid());
                    for(term; term<number; term++)
                    {
                        kill(array[term], SIGTERM);//terminited all the created child process(pid)
                        counter=counter-1;//decrease the counter because some child process was terminated by SIGTERM

                    }
                    mark=0;//set mark=0 for continueing with the normal creation process

                }
    }
    while(1)//check if the creating process finished
    {
        pid_t child_pid = wait(&status);
        if(child_pid == -1)//finished to creat child process
        {
            printf("parents[%d]:excution has completed\n ", getpid());//print out the massage of parent completed
            break;
        }
        else
        {
            counter++;// counter the number of child process terminated normally
        }
    }
    printf("\n");
    printf(" %d process terminated normally\n ", counter);

    for(int j=0; j<NSIG; j++) //old service handlers of all signals should be restored.
        signal(j,SIG_DFL);

    return 0;

}
